import React from 'react';

// import styles from './components/Mammal/Mammal';

// props is PROPERTIES 
const mammal = (props) => {
    return(
        <div className="mammal">
            <p onClick={props.click}>{props.name} like to eat {props.qtyFish} fish...That is  ALOT of Fish and MAKES THEM {props.action}!</p>

            <input type="text" onChange={props.changeName} value={props.name } />
        </div>
    );
    
}

export default mammal;